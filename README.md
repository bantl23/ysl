# Yaml Scripting Language (ysl)

The yaml scripting language is created to be a generic execution environment for any purpose. ysl gets its inspiration from ansible and gitlab-ci. Both provide a scripting-like environment. Ysl's aim is to make the scripting environment language agnostic. Only interperative languages will be supported.
